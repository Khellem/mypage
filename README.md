# Trabalho 03 | Página em HTML | Experimento das aulas de Arduino.

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação - na disciplina de Introdução a Engenharia. 

|Nome| gitlab user|
|---|---|
| Dhayane Toldo      | khellem |
| Bruno Guerreiro    | BrunoGuerreiro08 |
| Guilherme Marcante | GuilhermeMarcante |

# Documentação

A documentação do projeto pode ser acessada pelo link:

https://khellem.gitlab.io/mypage/ 

# Links Úteis

* [Tutorial HTML](http://pt-br.html.net/tutorials/html/)
* [Gnuplot](http://fiscomp.if.ufrgs.br/index.php/Gnuplot)